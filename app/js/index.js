import PriceSwitcher from 'PriceSwitcher';
import CardSlider from 'CardSlider';
import RandomDots from 'RandomDots';

new CardSlider($('.testimonials'), {
    dot: 'testimonials__dot',
    activeDot: 'testimonials__dot_active',
    card: 'testimonials__card',
    activeCard: 'testimonials__card_active',
    cardText: 'testimonials__text',
})

new CardSlider($('.tools'), {
    dot: 'tools__switcher',
    activeDot: 'tools__switcher_active',
    card: 'tools__content-slide',
    activeCard: 'tools__content-slide_active',
    cardText: 'tools__content-slide',
});

new PriceSwitcher($('.pricing'), {
    tab: 'pricing__tab',
    activeTab: 'pricing__tab_selected',
    oldPrice: 'card__label_top',
    oldPriceActive: 'card__label_top_visible',
    actualPrices: 'card__numbers',
})

new RandomDots($('#randomDots'), {
    count: 40,
    color:'#0f4c7b'
})
