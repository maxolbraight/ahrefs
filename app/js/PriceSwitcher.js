class PriceSwitcher{
    constructor($root, ui){
        this.root = $root;
        this.ui = ui;
        this.tabs = this.root.find('.' + this.ui.tab);
        this.oldPrices = this.root.find('.' + this.ui.oldPrice);
        this.actualPrices = this.root.find('.' + this.ui.actualPrices);
        this.addEventListeners();
    }

    addEventListeners(){
        this.root.on('click', '.' + this.ui.tab, this.onTabClick.bind(this));
    }

    onTabClick(e){
        let clickedTab = $(e.target).closest('.' + this.ui.tab);
        let priceMode = clickedTab.data('price-mode');

        this.toggleActiveTab(clickedTab);
        this.toggleOldPrice(priceMode);
        this.setActualPrices(priceMode);
    }

    setActualPrices(priceMode){
        this.actualPrices.each(function(){
            let price = $(this).data(priceMode);
            $(this).text(price);
        })
    }

    toggleOldPrice(priceMode){
        if(priceMode === 'annual'){
            this.oldPrices.addClass(this.ui.oldPriceActive);
        } else{
            this.oldPrices.removeClass(this.ui.oldPriceActive);
        }
    }

    toggleActiveTab(clickedTab){
        this.tabs.filter('.' + this.ui.activeTab).removeClass(this.ui.activeTab);
        clickedTab.addClass(this.ui.activeTab);
    }
}

export default PriceSwitcher;
