class RandomDots{
    constructor($root, settings){
        this.root = $root;
        this.context  = $root[0].getContext('2d');
        this.settings = settings;
        this.width = this.root.outerWidth();
        this.height = this.root.outerWidth();

        this.draw();
    }

    draw(){
        for(var i = 0; i < this.settings.count; i++){
            var randomPX= Math.floor((Math.random() * this.width) + 1);
            var randomPY= Math.floor((Math.random() * this.height) + 1);
            var randomSize= Math.floor((Math.random() * 20) + 5);

            this.context.fillStyle = this.settings.color;
            this.context.fillRect(randomPX, randomPY, randomSize, randomSize);
        }
    }
}

export default RandomDots;
