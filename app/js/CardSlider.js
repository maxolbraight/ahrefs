class CardSlider{
    constructor($root, ui){
        this.root = $root;
        this.ui = ui;
        this.cards = this.root.find('.' + this.ui.card);
        this.dots = this.root.find('.' + this.ui.dot);
        this.addEventListeners();
    }

    addEventListeners(){
        this.root.on('click', '.' + this.ui.cardText, this.onTextClick.bind(this));
        this.root.on('click', '.' + this.ui.dot, this.onDotClick.bind(this));
    }

    onDotClick(e){
        let clickedDot = $(e.target).closest('.' + this.ui.dot);
        let index = this.dots.index(clickedDot);
        this.setNewActiveCard(index);
    }

    onTextClick(){
        let nextCardIndex = this.getNextCardIndex();
        this.setNewActiveCard(nextCardIndex);
    }

    setNewActiveCard(index){
        this.clearCurrentActiveDot();
        this.clearCurrentActiveCard();
        this.addActiveToDot(index);
        this.addActiveToCard(index);
    }

    getNextCardIndex(){
        let countOfCards = this.cards.length;
        let index = this.cards.filter('.' + this.ui.activeCard).index();
        let nextCardIndex = index + 1 === countOfCards ? 0 : index + 1;

        return nextCardIndex;
    }

    clearCurrentActiveDot(){
        this.dots.filter('.' + this.ui.activeDot).removeClass(this.ui.activeDot);
    }

    clearCurrentActiveCard(){
        this.cards.filter('.' + this.ui.activeCard).removeClass(this.ui.activeCard);
    }

    addActiveToDot(dotIndex){
        this.dots.eq(dotIndex).addClass(this.ui.activeDot);
    }

    addActiveToCard(cardIndex){
        this.cards.eq(cardIndex).addClass(this.ui.activeCard);
    }
}

export default CardSlider;
