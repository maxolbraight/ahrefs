var beeper = require('beeper');

var handlers = {
    change: function(data){
        console.log(data.type, data.path);
    },
    error: function(data){
        if(data.fileName){
            console.log('Error: ' + data.fileName + ', line ' + data.lineNumber + ', column ' + data.column + ', message ' + data.message);
        }else{
            console.log(data.toString())
        }
        beeper('*');

        this.emit('end');
    }
}

module.exports = handlers;
