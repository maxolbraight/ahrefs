var config = require('../config');
var handlers = require('../handlers');

var gulp = require('gulp');
var pug = require('gulp-pug');

var fs = require("fs");

gulp.task('html', function () {
    var svgFile = ''
    var result = {};

    try {
        svgFile = fs.readFileSync(config.desc + '/images/sprite.svg', "utf-8");
    } catch (e) {
        svgFile = '';
    }

    result = {
        jsBundle: './js/' + config.js.compiled,
        cssBundle: './css/' + config.css.compiled,
        svg: svgFile
    };

    return gulp.src(config.html.src + '*.jade')
        .pipe(pug({
            data: result,
            pretty: true
        }))
        .on('error', handlers.error)
        .pipe(gulp.dest(config.html.desc));
});
