var config = require('../config');
var handlers = require('../handlers');

var gulp = require('gulp');
var rename = require('gulp-rename');

var csso = require('gulp-csso');

var base64 = require('gulp-base64');

var postcss = require('gulp-postcss');

var atImport = require('postcss-import');
var cssnext = require('postcss-cssnext');
var minifyFontValues = require('postcss-minify-font-values');
var inlineSVG = require('postcss-inline-svg');
var brandColors = require('postcss-brand-colors');
var size = require('postcss-size');

var modules = [
    atImport(config.atImport),
    size(),
    brandColors(),
    cssnext({
        browsers: ['last 4 version', '> 5%']
    }),
    minifyFontValues(),
    inlineSVG({
        path: './' + config.desc + '/'
    }),
];

gulp.task('css', function () {
    return gulp.src([
            config.css.src + config.css.startPoint
        ])
        .pipe(postcss(modules))
        .on('error', handlers.error)
        //.pipe(base64(config.assets))
        //.pipe(csso())
        .pipe(rename(config.css.compiled))
        .pipe(gulp.dest(config.css.desc));
});
