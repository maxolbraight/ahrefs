var config = require('../config');
var handlers = require('../handlers');
var runSequence = require('run-sequence');

var gulp = require('gulp');
var svgmin = require('gulp-svgmin');
var svgStore = require('gulp-svgstore');
var cheerio = require('gulp-cheerio');

gulp.task('image', function(callback){
    runSequence('copyImages', 'prepareSvg', 'makeSvgSprite', callback);
});

gulp.task('prepareSvg', function () {
    return gulp.src(config.images.src + '**/*.svg')
        .on('error', handlers.error)
        .pipe(gulp.dest(config.images.desc));
});

gulp.task('makeSvgSprite', function () {
    return gulp.src(config.images.desc + 'sprite/*.svg')
        .pipe(svgStore({ inlineSvg: true }))
        .pipe(cheerio({
            run: function ($) {
                $('[style]').removeAttr('style');
            },
            parserOptions: { xmlMode: true }
        }))
        .on('error', handlers.error)
        .pipe(gulp.dest(config.images.desc));
});

gulp.task('copyImages', function () {
    return gulp.src([config.images.src + '*.{png,gif,jpg,ico}'])
        .pipe(gulp.dest(config.images.desc));
});
