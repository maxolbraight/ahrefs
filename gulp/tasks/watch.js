var handlers = require('../handlers');
var config = require('../config');

var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('watch', function () {
    gulp.watch(config.css.src + '*.pcss', ['css'])
        .on('change', handlers.change);

    gulp.watch([config.js.src + '**/*.js', config.js.src + '**/*.jsx'], ['js'])
        .on('change', handlers.change);

    gulp.watch([config.html.src + '**/*.jade'], ['html'])
        .on('change', handlers.change);

     gulp.watch(config.images.src + '*', function(callback){
         runSequence('image', 'css', callback);
     });
});
