var config = require('../config');
var handlers = require('../handlers');
var gulp = require('gulp');
var gulpUtil = require('gulp-util');
var uglify = require('gulp-uglify');

var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

gulp.task('js', function(callback){
    return browserify({
            entries: config.js.src + config.js.startPoint,
            paths: ['./node_modules', config.js.src],
            debug: true
        })
        .transform('babelify', {
            presets: ['es2015'],
        })
        .bundle()
        .on('error', handlers.error)
        .pipe(source(config.js.compiled))
        .pipe(gulp.dest(config.js.desc));
});

gulp.task('js:min', function(callback){
    return browserify({
            entries: config.js.src + config.js.startPoint,
            paths: ['./node_modules', config.js.src]
        })
        .transform('babelify', {
            presets: ['es2015'],
        })
        .bundle()
        .on('error', handlers.error)
        .pipe(source(config.js.compiled))
        .pipe(buffer())
        .pipe(uglify().on('error', gulpUtil.log))
        .pipe(gulp.dest(config.js.desc));
});
