var config = require('../config');

var gulp = require('gulp');
var rimraf = require('gulp-rimraf');

gulp.task('clean', function(){
    return gulp.src([config.css.desc, config.fonts.desc, config.js.desc, config.images.desc])
        .pipe(rimraf());
});
