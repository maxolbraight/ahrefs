var config = {};

config.src = 'app';
config.desc =  'build';
config.deploy = 'deploy';

config.images = {
    src: config.src + '/images/',
    desc: config.desc + '/images/',
};

config.fonts ={
    src: config.src + '/fonts/',
    desc: config.desc + '/fonts/'
};

config.css = {
    src: config.src + '/pcss/',
    desc: config.desc + '/css/',
    startPoint: '_styles.pcss',
    compiled: 'app.bundle.css'
};

config.js = {
    src: config.src + '/js/',
    desc: config.desc + '/js/',
    startPoint: 'index.js',
    compiled: 'app.bundle.js'
};

config.html = {
    src: config.src + '/jade/',
    desc: config.desc,
    startPoint: '*.jade'
};

config.assets = {
    baseDir: '' + config.desc + '/',
    maxImageSize: 500 * 1024, //500kb
    debug: true
};

config.atImport = {
    path: [config.css.src]
};

config.imagemin = {
    progressive: true
};

module.exports = config;
